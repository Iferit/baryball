﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	[Header("Properties")]
	public static GameController ACTIVE;
	[SerializeField]
	public static List<Player> players;
	public static int activePlayer=0;

	// Use this for initialization
	private void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
	}

	void Start () {
		if (ACTIVE == null)
		{
			ACTIVE = this;
		}
		else
		{
			Destroy(this.gameObject);
		}

		players = new List<Player>();
	}
	
	public void AddScore(float s)
	{
		players[activePlayer].score += s;
	}

	public void NextPlayer()
	{
		activePlayer++;
		if (activePlayer > players.Count - 1)
		{
			ScoreGame();
		}
	}

	public void ScoreGame()
	{

	}
}
